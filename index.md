---
title: Automation for Dummies
subtitle: Focus on RedHat Ansible and Saltstack Salt
description:
author: Yann Masson
theme: moon
separator: <!--s-->
revealOptions:
  width: 1280
  height: 1024
  center: false
  transition: 'fade'
---

## Automation for Dummies
  Focus on RedHat Ansible and Saltstack Salt

<!--s-->
### Définition
  Automatisation :

  >  Un automate est un dispositif reproduisant en autonomie une séquence d'actions prédéterminées sans l'intervention humaine, le système fait toujours la même chose, ou s'adapte à des conditions environnementales perçues par ses capteurs. L'automate est un objet programmé.

  https://fr.wikipedia.org/wiki/Automate

<!--s-->
### Histoire
  Il était une fois les gestionnaires de configuration...

<!--s-->
### Histoire
  Il était une fois les gestionnaires de configuration...


  1993 : GNU/cfengine

<!--s-->
### Histoire
  Il était une fois les gestionnaires de configuration...


  1993 : GNU/cfengine

  2005 : Puppet (Puppet Labs)

<!--s-->
### Histoire
  Il était une fois les gestionnaires de configuration...


  1993 : GNU/cfengine

  2005 : Puppet (Puppet Labs)

  2009 : Chef (Opscode)

<!--s-->
### Histoire
  Il était une fois les gestionnaires de configuration...


  1993 : GNU/cfengine

  2005 : Puppet (Puppet Labs)

  2009 : Chef (Opscode)

  2011 : Salt (Saltstack)

<!--s-->
### Histoire
  Il était une fois les gestionnaires de configuration...


  1993 : GNU/cfengine

  2005 : Puppet (Puppet Labs)

  2009 : Chef (Opscode)

  2011 : Salt (Saltstack)

  2012 : Ansible (Ansible Inc, racheté par RedHat en oct. 2015)

<!--s-->
### Le coeur du code
 ![change_configs_compared](change_configs_compared.png)

<!--s-->
### Ansible
  Ansible is Simple IT Automation.

  https://www.ansible.com

<!--s-->
### Ansible, l'écosysème
  Ansible comprend plusieurs éléments:

  - Ansible, gestion de configuration

  - Ansible Galaxy, dépôt central de rôles et partage

  - Ansible Tower, gestion de déploiement complexe, fourni une API. (sous licence commerciale)

  - Ansible Network, gestion du réseau


<!--s-->
### Ansible, les bases
  - Pas d'agent installé sur les équipements managés

  - Utilise SSH pour se connecter aux équipements

  - Basé sur un fichier inventaire des équipements

<!--s-->
### Ansible, la mise en oeuvre
 ![ansible_1](ansible_1.png)

<!--s-->
### Salt
  SaltStack intelligent, event-driven IT automation software.

  https://www.saltstack.com/

<!--s-->
### Salt, l'écosystème
  Salt inclus de base beaucoup de fonctionnalités. Il est complété par quelques modules:

  - salt-master/salt-minion, gestion de configuration, event-driven, reactor, ect..

  - salt-api, API REST permettant de "piloter" le master

  - salt-cloud, module permettant le provisionning de Clouds Providers (AWS, Azure, GCE, Openstack, VMware, ect..)


  La version Entreprise fournie une IHM, console, module de `security compliance`

<!--s-->
### Salt, les bases
  - peut fonctionner avec ou sans master. Pour utiliser toutes les capacités, il faut un master

  - nécessite un agent sur chaque serveur (salt-minion)

  - pour les équipements réseaux : API, ssh ou agent

  - la communication entre le Master et les équipements se fait à travers un bus de messages, mais peut aussi utiliser SSH.

  - sans inventaire, basé sur des tags ou des variables

<!--s-->
### Salt, la mise en oeuvre
 ![salt_1](salt_1.png)

<!--s-->
### Si on devait les comparer
 ![simple_hard](simple_hard.jpeg)

<!--s-->
### Si on devait les comparer
||||
|---|---|---|
| | Ansible | Salt |
| <small>courbe d'apprentissage</small> | <small>facile</small> | <small>moyen</small> |
| <small>language</small> | <small>Python, YAML, Jinja</small> | <small>Python, YAML, Jinja</small> |
| <small>Master ?</small> | <small>non</small> | <small>oui et non</small> |
| <small>Connection</small> | <small>SSH</small> | <small>Bus ZeroMQ par défaut. SSH avec salt-ssh</small> |
| <small>Rapidité</small> | <small>lent à cause de SSH</small> | <small>très rapide grâce au bus ZeroMQ</small> |
| <small>Gestion de configuration</small> | <small>oui</small> | <small>oui</small> |

<!--s-->
### Si on devait les comparer
||||
|---|---|---|
| | Ansible | Salt |
| <small>Evite la dérive de la configuration</small> | <small>non (en natif)</small> | <small>oui</small> |
| <small>Event-Driven</small> | <small>non</small> | <small>oui</small> |
| <small>Orchestration</small> | <small>oui</small> | <small>oui</small> |
| <small>Cloud provisionning</small> | <small>oui avec les Clouds Modules</small> | <small>oui avec salt-cloud</small> |
| <small>Network managment</small> | <small>oui</small> | <small>oui</small> |
| <small>Version Entreprise</small> | <small>oui</small> | <small>oui</small> |

<!--s-->
### Exemple de Cas d'usages
  * Installer un serveur Worpress pour un client et lui fournir la procédure -> Ansible

  * Installer et gérer une plateforme en garantissant la non dérive, la sécurité et les livraisons -> Salt

<!--s-->

